package config

//const (
//	Paranoid       = iota
//	Normal         = iota
//	Sneaky         = iota
//	Log            = iota
//)

var (
	Policies = map[string]int{
		"log":      1,
		"normal":   2,
		"sneaky":   3,
		"paranoid": 4,
	}
)

func isValidPolicy(policy string) bool {
	isValidPolicy := false
	for validPolicy, _ := range Policies {
		if policy == validPolicy {
			isValidPolicy = true
			break
		}
	}

	return isValidPolicy
}

func GetPoliciesNames() []string {
	policies := make([]string, len(Policies))

	index := 0
	for name := range Policies {
		policies[index] = name
		index++
	}

	return policies
}
