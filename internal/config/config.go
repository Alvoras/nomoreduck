package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/go-yaml/yaml"
)

var (
	Cfg = new(Config)
)

// Config structure which mirrors the json file
type Config struct {
	Enabled           bool     `yaml:"Enabled"`
	Policy            string   `yaml:"Policy"`
	BlockingLog       bool     `yaml:"Blockinglog"`
	Threshold         int      `yaml:"Threshold"`
	Size              int      `yaml:"Size"`
	DropEvery         int      `yaml:"DropEvery"`
	LogFile           string   `yaml:"LogFile"`
	KeystrokeLogFile  string   `yaml:"KeystrokeLogFile"`
	LockScreenCommand []string `yaml:"LockScreenCommand"`
}

func (cfg *Config) Load() {
	filepath := "config.yml"

	cfg.Enabled = true
	cfg.BlockingLog = false
	cfg.Threshold = 30
	cfg.Size = 20
	cfg.DropEvery = 5
	cfg.LogFile = "nomoreduck.log"
	cfg.KeystrokeLogFile = "nmd_keystrokes.log"
	cfg.LockScreenCommand = []string{"l", "command"}

	cfgData, err := ioutil.ReadFile(filepath)
	if err != nil {
		fmt.Println(fmt.Sprintf("Failed to read config file at [%s]", filepath))
		fmt.Println(err)
		os.Exit(1)
	}
	if err := yaml.Unmarshal(cfgData, &cfg); err != nil {
		fmt.Printf("Failed to load the config file [%s]\n", filepath)
		fmt.Println(err)
		os.Exit(1)
	}

	if !isValidPolicy(cfg.Policy) {
		fmt.Println(fmt.Sprintf("%s is not a valid policy name [%s]", cfg.Policy, strings.Join(GetPoliciesNames(), ",")))
		os.Exit(1)
	}
}
