//+build windows

package keypress

import (
	// "fmt"
	"gitlab.com/Alvoras/nomoreduck/internal/keymap"
	"gitlab.com/Alvoras/nomoreduck/internal/user32"
	"time"
	"unsafe"
)

var (
	carryShift = false
)

type Keypress struct {
	Timestamp time.Time
	VkCode    uint
	ScanCode  uint
	ShiftOn   bool
	CapsOn    bool
}

func New(kbdstruct *user32.KBDLLHOOKSTRUCT) *Keypress {
	return &Keypress{
		Timestamp: time.Now(),
		VkCode:    uint(kbdstruct.VkCode),
		ScanCode:  uint(kbdstruct.ScanCode),
		ShiftOn:   user32.GetKeyState(user32.VK_SHIFT) < 0,
		CapsOn:    user32.GetKeyState(user32.VK_CAPITAL) < 0,
	}
}

func (kp *Keypress) VkToString() string {
	var ok bool

	if _, ok = keymap.Keymap[uint16(kp.VkCode)]; !ok {
		return "[?]"
	}

	// If either shift of capslock is on
	if kp.ShiftOn != kp.CapsOn {
		// if kp.VkCode > 48 && kp.VkCode < 91 {
		// }
		return keymap.Keymap[uint16(kp.VkCode)][keymap.Uppercase]
		//kp.VkCode += keymap.ShiftOffset
		// if kp.VkCode >= 65 && kp.VkCode <= 90 {
		// }
	}

	return keymap.Keymap[uint16(kp.VkCode)][keymap.Lowercase]
}

func (kp *Keypress) ToUnicode() string {
	lpKeyState := make([]byte, 256)
	var pwszBuff user32.WCHAR_T

	user32.GetKeyboardState(&lpKeyState)
	ok := user32.ToUnicode(kp.VkCode, kp.ScanCode, &lpKeyState[0], &pwszBuff, unsafe.Sizeof(pwszBuff), 2)

	switch ok {
	case -1, 0:
		return ""
	}

	return string(pwszBuff)
}
