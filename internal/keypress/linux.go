//+build linux

package keypress

import (
	"syscall"
	"unsafe"

	"gitlab.com/Alvoras/nomoreduck/internal/keymap"
)

type InputEvent struct {
	Time     syscall.Timeval
	Type     uint16
	Code     uint16
	Value    int32
}

var (
	shiftOn    bool
	capslockOn bool
)

const (
	InputEventSize = int(unsafe.Sizeof(InputEvent{}))
)

func (ev *InputEvent) KeycodeToString() string {
	var str string
	var ok bool

	// If either shift of capslock is on
	if shiftOn != capslockOn {
		if _, ok = keymap.Keymap[ev.Code]; !ok {
			str = "[?]"

		} else {
			str = keymap.Keymap[ev.Code][keymap.Uppercase]
		}
	} else {
		if _, ok = keymap.Keymap[ev.Code]; !ok {
			str = "[?]"
		} else {
			str = keymap.Keymap[ev.Code][keymap.Lowercase]
		}
	}

	if shiftOn && ev.Code != 42 {
		shiftOn = false
	} else {
		if ev.Code == 42 {
			shiftOn = true
			return ""
		}
	}

	if capslockOn && ev.Code != 58 {
		capslockOn = false
	} else {
		if ev.Code == 58 {
			capslockOn = true
			return ""
		}
	}

	println(ev.Code, str)
	return str
}

func (ev *InputEvent) ToString() string {
	return ev.KeycodeToString()
}
