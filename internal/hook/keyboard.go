package hook

import (
	"fmt"
	"time"

	"gitlab.com/Alvoras/nomoreduck/internal/config"
	"gitlab.com/Alvoras/nomoreduck/internal/device"

	// "fmt"
	"os"
)

type KeyboardHook struct {
	Devices          []*device.Device
	//KeystrokeLogFile *os.File
	BlockingLog      bool
	DropEvery        int
	// Policy           int
	SampleSize int
	Config     *config.Config
}

func New(cfg *config.Config) *KeyboardHook {
	//keystoreLogFile, err := os.OpenFile(time.Now().Format("2006-01-02"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	//if err != nil {
	//	fmt.Println(fmt.Sprintf("Failed to open the keystroke log file [%s]", cfg.KeystrokeLogFile))
	//}

	var kb = &KeyboardHook{
		//KeystrokeLogFile: keystoreLogFile,
		BlockingLog:      cfg.BlockingLog,
		DropEvery:        cfg.DropEvery,
		Config:           cfg,
	}

	return kb
}

func (kbh *KeyboardHook) Log(str string) {
	var f *os.File
	var err error
	now := time.Now().Format("2006_01_02-15-04-05")
	var dir = "logs"
	var filename = fmt.Sprintf("%s/%s", dir, now)

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			fmt.Println(fmt.Sprintf("Failed to create the log directory [%s]", dir), err)
		}
	}


	f, err = os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println(fmt.Sprintf("Failed to open the keystroke log file [%s]", filename, err))
		fmt.Println("Fallback to writing logs to :", now)
		f, err = os.OpenFile(now, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
		if err != nil {
			fmt.Println(fmt.Sprintf("Failed to open the keystroke log file [%s]", now, err))
			fmt.Println("Failed to write :")
			fmt.Println(str)
			return
		}
	}


	defer f.Close()

	_, _ = f.WriteString(str)
}
