//+build linux

package hook

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/go-vgo/robotgo"

	"github.com/fsnotify/fsnotify"
	"gitlab.com/Alvoras/nomoreduck/internal/config"
	"gitlab.com/Alvoras/nomoreduck/internal/device"
)

func (kbh *KeyboardHook) InitDevices() {
	grep := "/bin/grep"
	line := fmt.Sprintf("%s -E 'Handlers|EV=' /proc/bus/input/devices | %s -EB1 'EV=1[02]0013' | %s -Eo 'event[0-9]{1,2}'", grep, grep, grep)

	out, err := exec.Command("/bin/sh", "-c", line).Output()

	if err != nil {
		fmt.Println(err)
	}

	devs := strings.Split(strings.TrimRight(string(out), "\n"), "\n")

	for _, val := range devs {
		kbh.Devices = append(kbh.Devices, device.New(fmt.Sprintf("/dev/input/%s", val), kbh.Config))
		//fmt.Println(device.New(val, kbh.Config))
	}

	//fmt.Println(kbh.Devices, len(kbh.Devices))
}

func (kbh *KeyboardHook) IsRegisteredDevice(name string) (int, bool) {
	key := -1

	for key, dev := range kbh.Devices {
		if dev.Name == name {
			return key, true
		}
	}

	return key, false
}

func (kbh *KeyboardHook) DeleteAt(idx int) ([]*device.Device, bool) {
	if idx >= len(kbh.Devices) {
		return kbh.Devices, false
	}

	kbh.Devices[idx], kbh.Devices = kbh.Devices[len(kbh.Devices)-1], kbh.Devices[:len(kbh.Devices)-1]
	return kbh.Devices, true
}

func (kbh *KeyboardHook) StartDeviceWatcher(addDevice chan *device.Device, removeDevice chan *device.Device) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		fmt.Println("Failed to initialize the device watcher")
		os.Exit(1)
	}
	defer watcher.Close()

	if err := watcher.Add("/dev/input"); err != nil {
		if err != nil {
			fmt.Println("Failed to initialize the device watcher")
			os.Exit(1)
		}
	}

	for {
		select {
		case ev := <-watcher.Events:
			if ev.Name == "" {
				continue
			}
			switch ev.Op {
			case fsnotify.Create:
				if _, ok := kbh.IsRegisteredDevice(ev.Name); !ok {
					//fmt.Println("new dev", ev.Name)
					addDevice <- device.New(ev.Name, kbh.Config)
				}

			case fsnotify.Remove:
				//fmt.Println("removed", ev.Name)
				removeDevice <- device.New(ev.Name, kbh.Config)
			}
		case err := <-watcher.Errors:
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		}
	}
}

func (kbh *KeyboardHook) StartLoggers(alert chan *device.Event, unlock chan *device.Event) {
	for _, dev := range kbh.Devices {
		go dev.Listen(alert, unlock)
	}
}

func (kbh *KeyboardHook) Start() {
	unlock := make(chan *device.Event)
	alert := make(chan *device.Event)

	addDevice := make(chan *device.Device)
	removeDevice := make(chan *device.Device)

	kbh.InitDevices()
	go kbh.StartDeviceWatcher(addDevice, removeDevice)
	kbh.StartLoggers(alert, unlock)
	fmt.Println("Loggers started")

	for {
		select {
		case dev := <-addDevice:
			kbh.Devices = append(kbh.Devices, dev)
			go dev.Listen(alert, unlock)

		case dev := <-removeDevice:
			if idx, ok := kbh.IsRegisteredDevice(dev.Name); ok {
				kbh.Devices, _ = kbh.DeleteAt(idx)
			}
			dev.Close()

		case ev := <-alert:
			fmt.Println("ducky detected")

			switch config.Cfg.Policy {
			case "normal":
				_ = ev.Device.Grab()

			case "paranoid":
				//var path string
				//var err error
				modifiers := config.Cfg.LockScreenCommand[1:]
				robotgo.KeyTap(config.Cfg.LockScreenCommand[0], modifiers)
				_ = ev.Device.Grab()

				//path, err = exec.LookPath("xtrlock")
				//if err != nil {
				//	fmt.Println("Failed to lock the screen. xtrlock is missing")
				//	fmt.Println("Falling back to normal policy")
				//	config.Cfg.Policy = config.Normal
				//	return
				//}
				//
				//fmt.Println(os.Geteuid())
				//cmd := exec.Command("sudo", "-u", "", path)
				//cmd.Stdin = os.Stdin
				//cmd.Stderr = os.Stderr
				//fmt.Println(cmd.Args)
				//err = cmd.Start()
				//if err != nil {
				//	fmt.Println("Failed to lock the screen")
				//}
				//fmt.Println(cmd.Wait())

			case "sneaky":
				if ev.Device.GrabOneTrigger {
					_ = ev.Device.Release()
					ev.Device.GrabOneTrigger = false
				}

				if ev.Device.SneakyCnt == kbh.DropEvery {
					ev.Device.SneakyCnt = 0
					_ = ev.Device.Grab()
					ev.Device.GrabOneTrigger = true
				}

				ev.Device.SneakyCnt++

			case "log":
				if len(ev.Device.CachedString) == 0 {
					// Since the ring buffer will (after 20 keystrokes) always be at the front edge, move forward
					// one value to be at the first keystroke registered
					ev.Device.PassiveCache = ev.Device.PassiveCache.Move(1)
					for i := 0;i < ev.Device.PassiveCache.Len() ; i++ {
						ev.Device.CachedString += ev.Device.PassiveCache.Value.(string)
						ev.Device.PassiveCache = ev.Device.PassiveCache.Next()
					}
				}
				ev.Device.CachedString += ev.Keypress.ToString()
				if config.Cfg.BlockingLog {
					_ = ev.Device.Grab()
				}
			}

		case ev := <-unlock:
			if ev.Device.Locked {
				_ = ev.Device.Release()
			}
			ev.Device.Pardon()

			if config.Cfg.Policy == "log" {
				kbh.Log(ev.Device.CachedString)
				ev.Device.CachedString = ""
			}
		}

	}
}
