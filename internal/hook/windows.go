// +build windows

package hook

import (
	"C"
	"fmt"
	"time"
	"unsafe"

	"github.com/JamesHovious/w32"
	"gitlab.com/Alvoras/nomoreduck/internal/config"
	"gitlab.com/Alvoras/nomoreduck/internal/device"
	"gitlab.com/Alvoras/nomoreduck/internal/keypress"
	"gitlab.com/Alvoras/nomoreduck/internal/user32"
)

func (kbh *KeyboardHook) WhitelistedKey(vk user32.DWORD) bool {
	switch vk {
	case w32.VK_BACK, w32.VK_VOLUME_DOWN, w32.VK_VOLUME_UP:
		return true
	}

	return false
}

func (kbh *KeyboardHook) Start() {
	var kbHookFn user32.HHOOK
	// var mouseHookFn user32.HHOOK
	var msg user32.MSG
	//var keystroke string
	fmt.Printf("Policy : %s\n", kbh.Config.Policy)
	// userPwd := ""
	// block := false
	// prompted := false
	//loggedStr := ""

	// We use kbh.Devices[0] in windows mode because the keyboard hook is global
	kbh.Devices = []*device.Device{device.New("", kbh.Config)}

	kbHookFn = user32.SetWindowsHookEx(user32.WH_KEYBOARD_LL,
		(user32.HOOKPROC)(func(nCode int, wparam user32.WPARAM, lparam user32.LPARAM) user32.LRESULT {
			if nCode == 0 && wparam == user32.WM_KEYDOWN {
				kbdstruct := (*user32.KBDLLHOOKSTRUCT)(unsafe.Pointer(lparam))
				kp := keypress.New(kbdstruct)

				fmt.Println(kp.ToUnicode())

				// Register the delay with the timestamp of the last keystroke
				if !kbh.WhitelistedKey(kbdstruct.VkCode) {
					kbh.Devices[0].RegisterDelay()
				}

				kbh.Devices[0].MakeAverage()
				// fmt.Printf("Average time between keystrokes : %v\n", kbh.Devices[0].Average)
				kbh.Devices[0].RefreshPreviousTimestamp()
				kbh.Devices[0].Counter++

				if kbh.Devices[0].IsBelowAverage() || kbh.Devices[0].Locked == true {
					// Ducky detected
					// fmt.Println("Ducky detected")
					switch config.Cfg.Policy {
					case "normal":
						// Break the event chain
						return 1
					case "paranoid":
						// modifiers := config.Cfg.LockScreenCommand[1:]
						// robotgo.KeyTap(config.Cfg.LockScreenCommand[0], modifiers)

						user32.LockWorkStation()
						return 1

					case "sneaky":
						// modulo ? Meh whatever
						if kbh.Devices[0].SneakyCnt == kbh.DropEvery {
							kbh.Devices[0].SneakyCnt = 0
							return 1
						}
						kbh.Devices[0].SneakyCnt++

					case "log":
						// If not locked, lock the device and register the time to allow
						// timeout
						if kbh.Devices[0].DetectionTs.IsZero() {
							kbh.Devices[0].Locked = true
							kbh.Devices[0].DetectionTs = time.Now()
						}

						if kbh.Devices[0].Locked == true {
							// If the attack is ongoing and we're within the timeout time frame
							if kbh.Devices[0].IsBelowAverage() && time.Now().Sub(kbh.Devices[0].DetectionTs) < 2*time.Second {
								//if keystroke = kp.ToUnicode(); keystroke == ""{
								//	keystroke = kp.VkToString()
								//}

								kbh.Devices[0].CachedString += kp.VkToString()
								//kbh.Devices[0].CachedString += keystroke
								// fmt.Println(kbh.Devices[0].CachedString)
								if kbh.BlockingLog {
									return 1
								}

								return user32.CallNextHookEx(kbHookFn, nCode, wparam, lparam)
							}
							// The attack has ended or we've reached the timeout
							// Unlock the input device, log the buffered string and reset timeout
							kbh.Devices[0].Locked = false
							kbh.Log(kbh.Devices[0].CachedString)
							kbh.Devices[0].CachedString = ""
							kbh.Devices[0].DetectionTs = time.Time{}
						}
					}
				}
			}
			if kbh.Devices[0].Locked {
				return 1
			}
			return user32.CallNextHookEx(kbHookFn, nCode, wparam, lparam)
		}), 0, 0)

	// Acquisition loop
	for user32.GetMessage(&msg, 0, 0, 0) != 0 {
	}

	user32.UnhookWindowsHookEx(kbHookFn)
	kbh.Devices[0].Close()
	kbHookFn = 0
}
