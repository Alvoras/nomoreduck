package icontray

import (
	"fmt"
	"os"

	"github.com/getlantern/systray"
	"gitlab.com/Alvoras/nomoreduck/icon"
	"gitlab.com/Alvoras/nomoreduck/internal/config"
)

func Init() {
	systray.Run(onready, onexit)
}

func onexit() {
}

func onready() {
	systray.SetIcon(icon.EnabledData)
	systray.SetTitle("")
	systray.SetTooltip("NoMoreDuck")

	toggleEnabled := systray.AddMenuItem("", "Enable or disable NoMoreDuck")
	if config.Cfg.Enabled {
		systray.SetIcon(icon.EnabledData)
		toggleEnabled.SetTitle("Disable")
	} else {
		systray.SetIcon(icon.DisabledData)
		toggleEnabled.SetTitle("Enable")
	}

	systray.AddSeparator()
	logPolicy := systray.AddMenuItem("Log", "Set policy to log only")
	sneakyPolicy := systray.AddMenuItem("Sneaky", "Set policy to sneaky")
	normalPolicy := systray.AddMenuItem("Normal", "Set policy to normal")
	paranoidPolicy := systray.AddMenuItem("Paranoid", "Set policy to paranoid")
	systray.AddSeparator()

	toggleBlockingLogOn := "ON"
	toggleBlockingLogOff := "OFF"

	if config.Cfg.BlockingLog {
		toggleBlockingLogOn = "[ON]"
	} else {
		toggleBlockingLogOff = "[OFF]"
	}

	toggleBlockingLogString := "Toggle blocking log : %s | %s"

	toggleBlockingLog := systray.AddMenuItem(fmt.Sprintf(toggleBlockingLogString, toggleBlockingLogOn, toggleBlockingLogOff), "Toggle blocking log mode")
	systray.AddSeparator()

	Quit := systray.AddMenuItem("Quit", "Exit NoMoreDuck")

	policyChannels := map[string]*systray.MenuItem{
		"log":      logPolicy,
		"sneaky":   sneakyPolicy,
		"normal":   normalPolicy,
		"paranoid": paranoidPolicy,
	}

	for mode, policyChan := range policyChannels {
		if mode == config.Cfg.Policy {
			policyChan.Disable()
			break
		}
	}

	go func() {
		<-Quit.ClickedCh
		fmt.Println("Exiting...")
		systray.Quit()
		os.Exit(0)
	}()

	for {
		fmt.Println(config.Cfg.Policy)
		fmt.Println(config.Cfg.Enabled)
		select {
		case <-toggleEnabled.ClickedCh:
			if config.Cfg.Enabled {
				systray.SetIcon(icon.DisabledData)
				config.Cfg.Enabled = false
				toggleEnabled.SetTitle("Enable")
			} else {
				systray.SetIcon(icon.EnabledData)
				config.Cfg.Enabled = true
				toggleEnabled.SetTitle("Disable")
			}

		case <-toggleBlockingLog.ClickedCh:
			config.Cfg.BlockingLog = !config.Cfg.BlockingLog
			if config.Cfg.BlockingLog {
				toggleBlockingLogOn = "[ON]"
				toggleBlockingLogOff = "OFF"

			} else {
				toggleBlockingLogOn = "ON"
				toggleBlockingLogOff = "[OFF]"
			}
			toggleBlockingLog.SetTitle(fmt.Sprintf(toggleBlockingLogString, toggleBlockingLogOn, toggleBlockingLogOff))
		case <-logPolicy.ClickedCh:
			config.Cfg.Policy = "log"

			for _, policyChan := range policyChannels {
				if policyChan.Disabled() {
					policyChan.Enable()
					break
				}
			}

			if !logPolicy.Disabled() {
				logPolicy.Disable()
			}
		case <-normalPolicy.ClickedCh:
			config.Cfg.Policy = "normal"

			for _, policyChan := range policyChannels {
				if policyChan.Disabled() {
					policyChan.Enable()
					break
				}
			}

			if !normalPolicy.Disabled() {
				normalPolicy.Disable()
			}
		case <-sneakyPolicy.ClickedCh:
			config.Cfg.Policy = "sneaky"

			for _, policyChan := range policyChannels {
				if policyChan.Disabled() {
					policyChan.Enable()
					break
				}
			}

			if !sneakyPolicy.Disabled() {
				sneakyPolicy.Disable()
			}
		case <-paranoidPolicy.ClickedCh:
			config.Cfg.Policy = "paranoid"

			for _, policyChan := range policyChannels {
				if policyChan.Disabled() {
					policyChan.Enable()
					break
				}
			}

			if !paranoidPolicy.Disabled() {
				paranoidPolicy.Disable()
			}

		}
	}
}
