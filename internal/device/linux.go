//+build linux

package device

/*
 #include <linux/input.h>
*/
import "C"
import (
	"bytes"
	"encoding/binary"
	"fmt"
	"gitlab.com/Alvoras/nomoreduck/internal/config"
	"gitlab.com/Alvoras/nomoreduck/internal/keypress"
	"os"
	"syscall"
	"time"
	"unsafe"
)

const (
	EVIOCGRAB = C.EVIOCGRAB // grab/release device
	EV_KEY    = 0x01
	//EV_MSC    = 0x04
	//KEYDOWN   = 1
	KEYUP     = 0
)

type Event struct {
	Device   *Device
	Keypress keypress.InputEvent
}

func ioctl(fd uintptr, name uintptr, data unsafe.Pointer) syscall.Errno {
	_, _, err := syscall.RawSyscall(syscall.SYS_IOCTL, fd, name, uintptr(data))
	return err
}

func newEvent(dev *Device, ipEv keypress.InputEvent) *Event {
	return &Event{
		Device:   dev,
		Keypress: ipEv,
	}
}

func (dev *Device) Lock() {
	dev.Locked = true
}

func (dev *Device) Unlock() {
	dev.Locked = false
}

func (dev *Device) Spot() {
	dev.Spotted = true
}

func (dev *Device) Pardon() {
	dev.Spotted = false
}

// Grab the input device exclusively
func (dev *Device) Grab() error {
	if dev.Locked {
		return nil
	}

	// C code for ioctl
	// Any false value passed as the third argument will ungrab the device, any other will grab it
	//
	//static long evdev_do_ioctl(struct file *file, unsigned int cmd,
	//                       void __user *p, int compat_mode)
	//{
	//[…]
	// switch (cmd) {
	// […]
	// case EVIOCGRAB:
	//	 if (p)
	//		 return evdev_grab(evdev, client);
	//	 else
	//		 return evdev_ungrab(evdev, client);
	// […]
	// }
	//[…]
	//}
	grab := int(1)
	if err := ioctl(dev.File.Fd(), uintptr(EVIOCGRAB), unsafe.Pointer(&grab)); err != 0 {
		return err
	}

	fmt.Println("grabbed")
	dev.Lock()

	return nil
}

// Release a grabbed input device
func (dev *Device) Release() error {
	if !dev.Locked {
		return nil
	}

	if err := ioctl(dev.File.Fd(), uintptr(EVIOCGRAB), unsafe.Pointer(nil)); err != 0 {
		return err
	}

	fmt.Println("released")
	dev.Unlock()

	return nil
}

// See https://github.com/gvalkov/golang-evdev/blob/master/device.go

func (dev *Device) Listen(alert chan *Event, unlock chan *Event) {
	var err error
	var timer *time.Timer
	fmt.Printf("%s listening\n", dev.Name)

	buffer := make([]byte, keypress.InputEventSize)

	ipEv := keypress.InputEvent{}

	dev.File, err = os.Open(dev.Name)
	if err != nil {
		fmt.Printf("%s not found. Aborted listener\n", dev.Name)
		return
	}

	defer dev.File.Close()

	for {
		select {
		case <-dev.Kill:
			fmt.Printf("Device %s closing\n", dev.Name)
			return
		default:
			if !config.Cfg.Enabled{
				continue
			}
			// Check if there is something to read from the input device's file
			readLen, err := dev.File.Read(buffer)
			if err != nil {
				fmt.Printf("Device %s has been disconnected\n", dev.Name)
				dev.Close()
			}

			// Skip if nothing read
			if readLen <= 0 {
				continue
			}

			// Encode binary received into InputEvent struct
			if err := binary.Read(bytes.NewBuffer(buffer), binary.LittleEndian, &ipEv); err != nil {
				fmt.Println("Failed to read the received input event")
				os.Exit(1)
			}

			// Filter for key events (EV_KEY) and only keyups
			if ipEv.Type != EV_KEY || ipEv.Value != KEYUP {
				continue
			}

			fmt.Printf("%#v\n\n", ipEv)

			// We don't need to whitelist repeating keys such as backspace since we're only listening to
			// keyup events
			dev.RegisterDelay()

			dev.MakeAverage()
			fmt.Printf("Average time between keystrokes : %v, dev : %s\n", dev.Average, dev.Name)
			dev.RefreshPreviousTimestamp()
			dev.Counter++

			ev := newEvent(dev, ipEv)
			dev.AddKeystrokeToPassiveCache(ev.Keypress.ToString())

			switch dev.Spotted {
			case true:
				// Send alert if the threshold has been met and something happenned in the
				// last 2 seconds
				if dev.IsBelowAverage() {
					alert <- ev
					timer.Reset(time.Second * 2)
				} else {
					timer.Stop()
					unlock <- ev
				}
			case false:
				if dev.IsBelowAverage() {
					dev.Spot()
					timer = time.NewTimer(time.Second * 2)
					go func() {
						<-timer.C
						fmt.Println("unlock timer expired")
						unlock <- ev
					}()
					alert <- ev
				}
			}
		}
	}
}
