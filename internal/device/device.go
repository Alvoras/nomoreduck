package device

import (
	"container/ring"
	"os"
	"time"

	"gitlab.com/Alvoras/nomoreduck/internal/config"
)

type Device struct {
	Name           string
	Delays         *ring.Ring
	SneakyCnt      int
	DetectionTs    time.Time
	PreviousTs     time.Time
	Limit          time.Duration
	Counter        uint64
	Average        time.Duration
	File           *os.File
	Locked         bool
	Spotted        bool
	Kill           chan bool
	GrabOneTrigger bool
	CachedString   string
	PassiveCache   *ring.Ring
}

func New(inputPath string, cfg *config.Config) *Device {
	var dev = &Device{
		Name:           inputPath,
		Counter:        0,
		Delays:         ring.New(cfg.Size),
		PassiveCache:   ring.New(cfg.Size),
		Limit:          time.Millisecond * time.Duration(cfg.Threshold),
		Kill:           make(chan bool, 1),
		GrabOneTrigger: false,
	}

	// Init the first two values as 2 * limit, which should be enough to avoid
	//  conflict on the first keystrokes
	dev.Delays.Value = 2 * dev.Limit
	dev.Delays.Move(1).Value = 2 * dev.Limit

	// time.Time.Add() returns a time.Time
	// while time.Time.Sub() returns a time.Duration...
	dev.PreviousTs = time.Now().Add(-dev.Delays.Value.(time.Duration))

	return dev
}

func (dev *Device) AddKeystrokeToPassiveCache(keystroke string) {
	dev.PassiveCache.Move(int(dev.Counter)).Value = keystroke
}


func (dev *Device) CleanDelays() {
	// dev.Delays.Do(func(p interface{}) {
	// 	p = nil
	// })

	dev.Delays.Unlink(dev.Delays.Len())

	dev.Delays.Value = 2 * dev.Limit
	dev.Delays.Move(1).Value = 2 * dev.Limit
}

func (dev *Device) RegisterDelay() {
	dev.Delays.Move(int(dev.Counter)).Value = time.Now().Sub(dev.PreviousTs)
}

func (dev *Device) MakeAverage() {
	var total time.Duration
	var cnt = 0

	dev.Delays.Do(func(el interface{}) {
		if el != nil {
			total += el.(time.Duration)
			cnt++
		}
	})

	dev.Average = time.Duration(float64(total.Nanoseconds()) / float64(cnt))
}

func (dev *Device) IsBelowAverage() bool {
	if dev.Average-dev.Limit < 0 {
		return true
	}

	return false
}

func (dev *Device) RefreshPreviousTimestamp() {
	dev.PreviousTs = time.Now()
}

// func (dev *Device) RefreshDetectionTimestamp() {
// 	dev.DetectionTs = time.Now()
// }
//
// func (dev *Device) ResetDetectionTimestamp() {
// 	dev.DetectionTs = time.Time{}
// }

func (dev *Device) Close() {
	dev.Kill <- true
}
