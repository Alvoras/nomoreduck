package service

import (
	"github.com/kardianos/service"
	"gitlab.com/Alvoras/nomoreduck/internal/config"
	"gitlab.com/Alvoras/nomoreduck/internal/hook"
)

type Program struct {
	Config *config.Config
}

func (p *Program) Start(s service.Service) error {
	// Start should not block. Do the actual work async.
	go p.run()
	return nil
}

func (p *Program) run() {
	var KeyboardHook = hook.New(p.Config)
	KeyboardHook.Start()
}

func (p *Program) Stop(s service.Service) error {
	// Stop should not block. Return with a few seconds.
	return nil
}
