# NoMoreDuck v1.1b
RubberDucky countermeasure inspired by the https://github.com/pmsosa/duckhunt project.

---

# Requirements
You need the libappindicator3-dev and the libxkbcommon-x11-dev to compile the binary on linux.
> sudo apt install libappindicator3-dev libxkbcommon-x11-dev libx11-xcb-dev

# TODO
+ [x] Config file
+ Windows
  + [x] Paranoid
  + [x] Normal
  + [x] Sneaky
  + [x] Log
+ Linux
  + [x] Paranoid
  + [x] Normal
  + [x] Sneaky
  + [x] Log
+ Tray bar icon
    + [ ] Presets for the buffer's threshold/size in the dropdown menu
    
# Known bugs
### Windows
+ Keyup event not firing for the shift key, resulting in a continuous input.

### Linux
+ Keyup event not firing sometimes after releasing a device, resulting in a continuous input. 

These two bugs are fixed by tapping the affected key, which reset its state.
