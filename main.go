package main

import (
	"gitlab.com/Alvoras/nomoreduck/internal/config"
	"gitlab.com/Alvoras/nomoreduck/internal/hook"
	"gitlab.com/Alvoras/nomoreduck/internal/icontray"
	//svc "gitlab.com/Alvoras/nomoreduck/internal/service"
)

func init() {
	config.Cfg.Load()
	go icontray.Init()
}

func main() {
	//svcConfig := &service.Config{
	//	Name:        "NoMoreDuck",
	//	DisplayName: "NoMoreDuck",
	//	Description: "Prevents automatic keystroke injection attacks",
	//}
	//
	//prg := &svc.Program{
	//	Config: cfg,
	//}
	//
	//s, _ := service.New(prg, svcConfig)
	//
	//s.Run()

	var KeyboardHook = hook.New(config.Cfg)
	KeyboardHook.Start()
}
